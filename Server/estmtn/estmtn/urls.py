from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import settings
from projects.views import DashboardView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'estmtn.views.home', name='home'),
    url(r'^projects/', include('projects.urls')),

    url(r'^accounts/', include('registration.backends.default.urls')),


    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', DashboardView.as_view(), name='dashboard'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.map_path('media'),
            }),
    )
