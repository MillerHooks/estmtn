__author__ = 'miller'

from django import forms
from projects.models import Requirement

class RequirementForm(forms.ModelForm):
    class Meta:
        model = Requirement