# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q

__admin__ = ('Project', 'Resource', 'Bucket', 'Requirement', 'EstimationMetric')

class Project(models.Model):
    """
    Top Level Component

    """
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name

class Resource(models.Model):
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class Bucket(models.Model):
    """
    Buckets

    """
    name = models.CharField(max_length=255)
    project = models.ForeignKey(Project, blank=True, null=True)
    photo = models.ImageField(upload_to='uploads/photos', blank=True)
    description = models.TextField(blank=True)
    deactivate = models.BooleanField()

    def __unicode__(self):
        return self.name

    def estimated(self):
        all_reqs = Requirement.objects.filter(core_feature=self).count()
        est_reqs = Requirement.objects.filter(~Q(points=''), core_feature=self).count()
        return str(est_reqs) + " / " + str(all_reqs)

DIFFICULTY_RATING= (
    ('0', '0'),
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('5', '5'),
    ('8', '8'),
    ('x', u"\u221E"),
    )

STATUS_RATING= (
    ('1', 'Need'),
    ('2', 'Want'),
    ('3', 'Nice to Have'),
    ('5', 'Deprecated/Broken/NA'),
    )

class Requirement(models.Model):
    """
    Requirement for feature

    """

    name = models.CharField(max_length=255)
    points = models.CharField(max_length=2, choices=DIFFICULTY_RATING, blank=True)
    priority = models.CharField(max_length=2, choices=STATUS_RATING, blank=True)
    description = models.TextField(blank=True)
    notes = models.TextField(blank=True)
    deactivate = models.BooleanField()

    resources = models.ManyToManyField(Resource, blank=True, null=True)
    buckets = models.ManyToManyField(Bucket, blank=True, null=True)
    prerequisites = models.ManyToManyField('Requirement', blank=True, null=True)


    min = models.IntegerField(blank=True, null=True)
    exp = models.IntegerField(blank=True, null=True)
    max = models.IntegerField(blank=True, null=True)

    @property
    def effort(self):
        try:
            effort = int(self.points) * self.estimation_metric.hours_per_point
        except:
            effort = u"\u221E"
        return effort

    def __unicode__(self):
        return self.name



class EstimationMetric(models.Model):
    """
    Model for valuation of points

    """
    name = models.CharField(max_length=255, blank=True)
    hours_per_point = models.IntegerField()
    points_per_week = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.hours_per_point + " hours per point"


