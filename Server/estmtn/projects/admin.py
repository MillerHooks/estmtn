from new import classobj
from django.contrib import admin

import projects.models as p_models

__custom_admins__ = {
    }

for model in p_models.__admin__:
    params = [getattr(p_models, model)]
    if __custom_admins__.has_key(model):
        params.append(__custom_admins__[model])
    else:
        _dyn_class = classobj('%sAdmin' % (model,),
            (admin.ModelAdmin,), {})
        #( VersionAdmin, ), {} )
        params.append(_dyn_class)
    admin.site.register(*params)
