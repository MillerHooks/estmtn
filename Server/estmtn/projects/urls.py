from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from projects import views as p_views

urlpatterns = patterns('',
    url(r'^$', p_views.ListRequirementView.as_view(), name='list_requirement'),
    url(r'^create/$', p_views.CreateRequirementView.as_view(), name='create_requirement'),
    url(r'^(?P<pk>\-?\d+)/$', p_views.DetailRequirementView.as_view(), name='detail_requirement'),
    url(r'^(?P<pk>\-?\d+)/edit/$', p_views.UpdateRequirementView.as_view(), name='update_requirement'),
    url(r'^estimate/(?P<pk>\-?\d+)/$', p_views.DetailRequirementView.as_view(), name='estimate'),

)
