from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from projects.models import Requirement, Project
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views import generic

from projects import forms as p_forms
from projects import models as p_models


class LoginRequiredMixin(object):
    """
    View mixin which verifies that the user has authenticated.

    NOTE:
        This should be the left-most mixin of a view.
    """

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class DashboardView(LoginRequiredMixin, generic.ListView):
    """
    The main view with a top down view

    """
    context_object_name = "estimated_stories"
    template_name = "projects/dashboard.html"
    queryset = p_models.Requirement.objects.exclude(points=u'')


class CommonRequirementMixin(object):
    template_name = "projects/%s_%s_view.html"
    project = None

    def get_queryset(self):
        filter_args = {'project': self.project} if self.project else {}
        self.queryset = p_models.Requirement.objects.all().filter(**filter_args)
        return super(CommonRequirmentMixin, self).get_queryset()


class ListRequirementView(CommonRequirementMixin, generic.ListView):
    template_name = "projects/requirement_list.html"

class DetailRequirementView(CommonRequirementMixin, generic.DetailView):
    template_name = "projects/requirement_detail.html"

class CreateRequirementView(CommonRequirementMixin, generic.CreateView):
    template_name = "projects/requirement_create.html"

class UpdateRequirementView(CommonRequirementMixin, generic.UpdateView):
    template_name = "projects/requirement_update.html"


class EstimateView(LoginRequiredMixin, generic.UpdateView):
    context_object_name = "stories"
    template_name = "projects/estimator.html"
    model = Requirement
    form_class = p_forms.RequirementForm

    def get_context_data(self, **kwargs):
        context = super(EstimateView, self).get_context_data(**kwargs)
        paginator = Paginator(self.queryset,1)

        #Stuff for pagination... doing it wrong?
        try:
            stories = (Requirement.objects.get(id=self.kwargs['pk']), True)
        except:
            page = self.request.GET.get('page')

            try:
                stories = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                stories = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                stories = paginator.page(paginator.num_pages)

        context['form'] = UserStoryForm(instance=stories[0])
        context['estimated_stories'] = UserStory.objects.exclude(points=u'')
        context['story_id'] = stories[0].id

        context['stories'] = stories
        return context