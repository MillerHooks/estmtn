# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Project'
        db.create_table('projects_project', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('projects', ['Project'])

        # Adding model 'Resource'
        db.create_table('projects_resource', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('projects', ['Resource'])

        # Adding model 'Bucket'
        db.create_table('projects_bucket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Project'], null=True, blank=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('deactivate', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('projects', ['Bucket'])

        # Adding model 'Requirement'
        db.create_table('projects_requirement', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('points', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
            ('priority', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('notes', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('deactivate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('min', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('exp', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('max', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('projects', ['Requirement'])

        # Adding M2M table for field resources on 'Requirement'
        db.create_table('projects_requirement_resources', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('requirement', models.ForeignKey(orm['projects.requirement'], null=False)),
            ('resource', models.ForeignKey(orm['projects.resource'], null=False))
        ))
        db.create_unique('projects_requirement_resources', ['requirement_id', 'resource_id'])

        # Adding M2M table for field buckets on 'Requirement'
        db.create_table('projects_requirement_buckets', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('requirement', models.ForeignKey(orm['projects.requirement'], null=False)),
            ('bucket', models.ForeignKey(orm['projects.bucket'], null=False))
        ))
        db.create_unique('projects_requirement_buckets', ['requirement_id', 'bucket_id'])

        # Adding model 'EstimationMetric'
        db.create_table('projects_estimationmetric', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('hours_per_point', self.gf('django.db.models.fields.IntegerField')()),
            ('points_per_week', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('projects', ['EstimationMetric'])


    def backwards(self, orm):
        # Deleting model 'Project'
        db.delete_table('projects_project')

        # Deleting model 'Resource'
        db.delete_table('projects_resource')

        # Deleting model 'Bucket'
        db.delete_table('projects_bucket')

        # Deleting model 'Requirement'
        db.delete_table('projects_requirement')

        # Removing M2M table for field resources on 'Requirement'
        db.delete_table('projects_requirement_resources')

        # Removing M2M table for field buckets on 'Requirement'
        db.delete_table('projects_requirement_buckets')

        # Deleting model 'EstimationMetric'
        db.delete_table('projects_estimationmetric')


    models = {
        'projects.bucket': {
            'Meta': {'object_name': 'Bucket'},
            'deactivate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['projects.Project']", 'null': 'True', 'blank': 'True'})
        },
        'projects.estimationmetric': {
            'Meta': {'object_name': 'EstimationMetric'},
            'hours_per_point': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'points_per_week': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'projects.project': {
            'Meta': {'object_name': 'Project'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'projects.requirement': {
            'Meta': {'object_name': 'Requirement'},
            'buckets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['projects.Bucket']", 'null': 'True', 'blank': 'True'}),
            'deactivate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'exp': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'min': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'points': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'resources': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['projects.Resource']", 'null': 'True', 'blank': 'True'})
        },
        'projects.resource': {
            'Meta': {'object_name': 'Resource'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        }
    }

    complete_apps = ['projects']