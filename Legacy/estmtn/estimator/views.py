from django.views.generic import ListView, DetailView
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect

from estimator.models import CoreFeature, Requirement, \
    Component, LegacyNode, UserStory, Page
from estimator.forms import RequirementForm, UserStoryForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q

from django.contrib.admin.options import IncorrectLookupParameters
from django.contrib.admin.views.main import ChangeList
from django.http import HttpResponse, HttpResponseForbidden, Http404
from django.core.serializers import serialize
from django.template import loader, RequestContext
from django.template.defaultfilters import slugify
from django.db.models.loading import get_model
from django.utils.encoding import smart_str
import csv

__all__ = ('DashboardView', )

class LoginRequiredMixin(object):
    """
    View mixin which verifies that the user has authenticated.

    NOTE:
        This should be the left-most mixin of a view.
    """

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)

class DashboardView(LoginRequiredMixin, ListView):
    """
    The main view with a top down view

    """
    context_object_name = "estimated_stories"
    template_name = "estimator/dashboard.html"
    queryset = UserStory.objects.exclude(points=u'')

class FeatureView(LoginRequiredMixin, ListView):
    """
    The main view with the pictures.

    """
    context_object_name = "requirement_list"
    template_name = "estimator/feature.html"

    def post(self, request, *args, **kwargs):
        form = RequirementForm(request.POST,)
        if form.is_valid():
            form.save()
            # do something.
        else:
            form = RequirementForm()

        return self.get(request, *args, **kwargs)

    def get_queryset(self):
        self.core_feature = get_object_or_404(CoreFeature, id=self.args[0])
        return Requirement.objects.filter(core_feature=self.core_feature)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(FeatureView, self).get_context_data(**kwargs)
        # Add in the publisher
        context['core_feature'] = self.core_feature
        #context['component_list'] = Component.objects.all()
        context['estimated_stories'] = UserStory.objects.exclude(points=u'')
        context['form'] = RequirementForm()
        return context

class RequirementDetailView(LoginRequiredMixin, DetailView):
    """
    The main view with the pictures.

    """
    context_object_name = "requirement"
    template_name = "estimator/requirement.html"
    queryset = Requirement.objects.all()

    def post(self, request, *args, **kwargs):
        form = RequirementForm(request.POST, instance=get_object_or_404(Requirement, id=self.kwargs['pk']))
        if form.is_valid():
            requirement = form.save(commit=False)
            requirement.core_feature = CoreFeature.objects.get(id=request.POST['core_feature'])

            requirement.save()
            # do something.
        else:
            form = RequirementForm()

        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(RequirementDetailView, self).get_context_data(**kwargs)
        #context['component_list'] = Component.objects.all()
        context['estimated_stories'] = UserStory.objects.exclude(points=u'')
        requirement = get_object_or_404(Requirement, id=self.kwargs['pk'])
        context['form'] = RequirementForm(instance=requirement)
        return context

class LegacyPageTreeListView(LoginRequiredMixin, ListView):

    context_object_name = "pages"
    template_name = "estimator/legacy_tree.html"
    queryset = LegacyNode.objects.all()

    def get_context_data(self, **kwargs):
        context = super(LegacyPageTreeListView, self).get_context_data(**kwargs)

        #context['component_list'] = Component.objects.all()
        context['estimated_stories'] = UserStory.objects.exclude(points=u'')
        context['nodes'] = self.queryset
        context['form_count'] = LegacyNode.objects.filter(form=True).count()
        return context

class PageTreeListView(LoginRequiredMixin, ListView):
    context_object_name = "pages"
    template_name = "estimator/page_tree.html"
    queryset = Page.objects.all()

    def get_context_data(self, **kwargs):
        context = super(PageTreeListView, self).get_context_data(**kwargs)
        stories = UserStory.objects.all()
        points = 0
        for story in stories:
            try:
                points = points+int(story.points)
            except:
                pass
        hours = points * 5

        #context['component_list'] = Component.objects.all()
        context['estimated_stories'] = UserStory.objects.exclude(points=u'')
        context['nodes'] = self.queryset
        context['total_hours'] = hours
        context['form_count'] = LegacyNode.objects.filter(form=True).count()
        return context

class EstimateView(LoginRequiredMixin, ListView):
    context_object_name = "stories"
    template_name = "estimator/estimator.html"
    queryset = UserStory.objects.filter(points='')

    def post(self, request, *args, **kwargs):
        story = get_object_or_404(UserStory, id=request.POST.get('id'))
        form = UserStoryForm(request.POST, instance=story)
        if form.is_valid():
            form.save()
        else:
            form = RequirementForm(instance=story)

        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EstimateView, self).get_context_data(**kwargs)
        paginator = Paginator(self.queryset,1)

        try:
            stories = (UserStory.objects.get(id=self.kwargs['pk']), True)
        except:
            page = self.request.GET.get('page')
        
            try:
                stories = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                stories = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                stories = paginator.page(paginator.num_pages)

        context['form'] = UserStoryForm(instance=stories[0])
        context['estimated_stories'] = UserStory.objects.exclude(points=u'')
        context['story_id'] = stories[0].id

        context['stories'] = stories
        return context

class EstimationResultView(LoginRequiredMixin, ListView):
    context_object_name = "stories"
    template_name = "estimator/legacy_tree_doc.html"
    queryset = UserStory.objects.exclude(points=u'')

    def get_context_data(self, **kwargs):
        context = super(EstimationResultView, self).get_context_data(**kwargs)
        stories = UserStory.objects.exclude(points=u'').exclude(points=u'x')
        #import pdb; pdb.set_trace()
        total_points = 0
        for story in stories:
            total_points = total_points+int(story.points)
        context['total_points'] = total_points
        return context


class CSVExprotView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        c = RequestContext(self.request)
        model = UserStory
        #writer = csv.writer(open('media/documents/userstories.csv', 'wb'), quoting=csv.QUOTE_ALL)
        writer = []
        headers = ['Story ID', 'User Role', 'Story', 'Points', 'Status', 'Notes' ]
        writer.append(headers)
        for obj in model.objects.all().order_by("id"):
            row = [obj.story_id, obj.user_role.name, obj.name, obj.points, obj.get_status_display(), obj.notes]

            writer.append(row)

        t = loader.get_template('admin/export/csv')
        c['rows'] = writer
        responseContents = t.render(c)
        
        response = HttpResponse(responseContents, mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=user_stories.csv'

        return response