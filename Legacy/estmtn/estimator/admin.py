from django.contrib import admin

from estimator.models import CoreFeature, Requirement, \
    Component, EstimationMetric, LegacyNode, UserStory, \
    UserRole, Page

class CoreFeatureAdmin(admin.ModelAdmin):
    list_display = ('name', 'component')
#    exclude = ('coordinates', 'location')
    search_fields = ('name', 'component')
    filter_horizontal = ('legacy_nodes',)

class RequirementAdmin(admin.ModelAdmin):
    list_display = ('name', 'core_feature', 'points')
    search_fields = ('name', 'core_feature')

class LegacyNodeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'parent', 'form')
    #search_fields = ('name', 'description')

class UserStoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'story_id', 'name', 'points', 'priority')
    filter_horizontal = ('pages', 'duplicates')

admin.site.register(Component)

admin.site.register(LegacyNode, LegacyNodeAdmin)
admin.site.register(CoreFeature, CoreFeatureAdmin)
admin.site.register(UserStory, UserStoryAdmin)
admin.site.register(UserRole)
admin.site.register(Page, LegacyNodeAdmin)

admin.site.register(Requirement, RequirementAdmin)
admin.site.register(EstimationMetric)
