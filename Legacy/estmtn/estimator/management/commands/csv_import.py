from django.core.management.base import BaseCommand, CommandError
from estimator.models import UserStory, UserRole
import csv

class Command(BaseCommand):
    args = 'file'
    help = 'Imports CSV into something'

    def handle(self, *args, **options):
        ifile  = open(args[0], "rb")
        reader = csv.reader(ifile)

        rownum = 0
        for row in reader:
            # Save header row.
            if rownum == 0:
                header = row
            else:
                colnum = 0
                notes = ''
                story_id = ''
                goal = ''
                role = ''
                priority = ''
                in_scope = False
                in_prd = False
                points = ''
                follow_up = ''
                min = 0
                exp = 0
                max = 0
                for col in row:
                    if colnum == 0:
                        story_id = col
                    elif colnum == 1:
                        priority = col
                    elif colnum == 2:
                        if col == 'y':
                            in_scope = True
                    elif colnum == 3:
                        if col == 'y':
                            in_prd = True
                    elif colnum == 4:
                        role = col
                    elif colnum == 5:
                        goal = col
                    elif colnum == 7:
                        points = col
                    elif colnum == 10:
                        notes = col
                    elif colnum == 11:
                        follow_up = col
                    elif colnum == 12:
                        min = col
                    elif colnum == 13:
                        exp = col
                    elif colnum == 14:
                        max = col

                    #print '%-8s: %s' % (header[colnum], col)
                    colnum += 1
                try:
                    role_obj = UserRole.objects.get_or_create(name=role)[0]
                    story = UserStory.objects.get_or_create(story_id=story_id)
                    print story[1]
                    story = story[0]

                    #import pdb; pdb.set_trace()
                    story.name = goal
                    story.priority = priority
                    story.in_scope = in_scope
                    story.in_prd = in_prd
                    #story.user_role = role_obj
                    story.story_id = story_id
                    story.notes = notes
                    story.follow_up = follow_up
                    try:
                        story.min = int(min)
                        story.save()
                    except:
                        pass
                    try:
                        story.exp = int(exp)
                        story.save()
                    except:
                        pass
                    try:
                        story.max = int(max)
                        story.save()
                    except:
                        pass
                    story.points = points

                    story.save()
                except Exception, e:
                    print e




            rownum += 1

        ifile.close()