from django.core.management.base import BaseCommand, CommandError
from estimator.models import UserStory, UserRole
import csv
from django.http import HttpResponse
from django.template.defaultfilters import slugify

class Command(BaseCommand):
    args = 'file'
    help = 'Imports CSV into something'

    def handle(self, *args, **options):
        model = UserStory
        writer = csv.writer(open('media/documents/userstories.csv', 'wb'), quoting=csv.QUOTE_ALL)
        headers = ['Story ID', 'User Role', 'Story', 'Points', 'Status', 'Notes' ]
        writer.writerow(headers)
        # Write data to CSV file
        count = 0
    	for obj in model.objects.all().order_by("id"):
            row = [obj.story_id, obj.user_role.name, obj.name, obj.points, obj.get_status_display(), obj.notes]
            print row
            writer.writerow(row)
            count = count+1

        print count
