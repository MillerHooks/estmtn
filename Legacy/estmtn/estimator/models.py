from django.db import models
from django.db.models import Q
from mptt.models import MPTTModel, TreeForeignKey
from django.db.models import Sum, Avg, Max

#from docx.docx import *

class Component(models.Model):
    """
    Top Level Component

    """
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name

class CoreFeature(models.Model):
    """
    Feature for components

    """
    name = models.CharField(max_length=255)
    component = models.ForeignKey(Component)
    photo = models.ImageField(upload_to='uploads/photos', blank=True)
    description = models.TextField(blank=True)
    deactivate = models.BooleanField()

    legacy_nodes = models.ManyToManyField("LegacyNode", blank=True, null=True)

    def __unicode__(self):
        return self.name

    def estimated(self):
        all_reqs = Requirement.objects.filter(core_feature=self).count()
        est_reqs = Requirement.objects.filter(~Q(points=''), core_feature=self).count()
        return str(est_reqs) + " / " + str(all_reqs)

DIFFICULTY_RATING= (
    ('0', '0'),
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('5', '5'),
    ('9', '9'),
    ('x', u"\u221E"),
)

STATUS_RATING= (
    ('1', 'Need'),
    ('2', 'Want'),
    ('3', 'Nice to Have'),
    ('5', 'Deprecated/Broken/NA'),
)

class Requirement(models.Model):
    """
    Requirement for feature

    """

    name = models.CharField(max_length=255)
    core_feature = models.ForeignKey(CoreFeature, blank=True, null=True)
    points = models.CharField(max_length=2, choices=DIFFICULTY_RATING, blank=True)
    priority = models.CharField(max_length=2, choices=STATUS_RATING, blank=True)
    description = models.TextField(blank=True)
    notes = models.TextField(blank=True)
    deactivate = models.BooleanField()

    def __unicode__(self):
        return self.name


    
class EstimationMetric(models.Model):
    """
    Model for valuation of points

    """
    name = models.CharField(max_length=255, blank=True)
    hours_per_point = models.IntegerField()
    points_per_week = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return u"%04i hours per point" % self.hours_per_point


class LegacyNode(MPTTModel):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True)
    site_photo = models.ImageField(upload_to='uploads/site_photos', blank=True)
    parent = TreeForeignKey("LegacyNode", blank=True, null=True, related_name='children')
    url = models.CharField(max_length=255, blank=True)
    form = models.BooleanField()

    def __unicode__(self):
        description = ''
        if self.description:
            description = "(%s)" % self.description

        if self.parent == None:
            return "%s %s" % (self.name, description)
        else:
            parent_description = ''
            if self.description:
                parent_description = "(%s)" % self.parent.description
            return "%s %s - %s %s" % (self.parent.name, parent_description, self.name, description)

    def count_forms(self):
        return LegacyNode.objects.filter(form=True).count()

    class MPTTMeta:
        order_insertion_by = ['name']


class Page(MPTTModel):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True)
    site_photo = models.ImageField(upload_to='uploads/site_photos', blank=True)
    parent = TreeForeignKey("Page", blank=True, null=True, related_name='children')
    url = models.CharField(max_length=255, blank=True)
    form = models.BooleanField()

    def __unicode__(self):
        description = ''
        if self.description:
            description = "(%s)" % self.description

        if self.parent == None:
            return "%s %s" % (self.name, description)
        else:
            parent_description = ''
            if self.description:
                parent_description = "(%s)" % self.parent.description
            return "%s %s - %s %s" % (self.parent.name, parent_description, self.name, description)

    def estimate(self):
        stories = UserStory.objects.filter(pages=self)
        points = 0
        for story in stories:
            try:
                points = points+int(story.points)
            except:
                pass
        hours = points * 5
        if points:
            return str(hours) + 'hrs'
        else:
            return None

    def count_forms(self):
        return LegacyNode.objects.filter(form=True).count()

    class MPTTMeta:
        order_insertion_by = ['name']


class UserRole(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name
    
class UserStory(Requirement):
    story_id = models.CharField(max_length=255)
    user_role = models.ForeignKey(UserRole)

    in_scope = models.BooleanField()
    in_prd = models.BooleanField()

    follow_up = models.TextField(blank=True)

    duplicates = models.ManyToManyField("UserStory", blank=True, null=True)
    pages = models.ManyToManyField(Page, blank=True, null=True)
    estimation_metric = models.ForeignKey(EstimationMetric, blank=True, null=True)

    min = models.IntegerField(blank=True, null=True)
    exp = models.IntegerField(blank=True, null=True)
    max = models.IntegerField(blank=True, null=True)

    @property
    def effort(self):
        try:
            effort = int(self.points) * self.estimation_metric.hours_per_point
        except:
            effort = u"\u221E"
        return effort


    def __unicode__(self):
        return self.name
    
class LegacyBucket(models.Model):
    node = models.ForeignKey(LegacyNode)

    def __unicode__(self):
        return self.node.name

