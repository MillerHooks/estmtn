# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Component'
        db.create_table('estimator_component', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('estimator', ['Component'])

        # Adding model 'CoreFeature'
        db.create_table('estimator_corefeature', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('component', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estimator.Component'])),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('deactivate', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('estimator', ['CoreFeature'])

        # Adding M2M table for field legacy_pages on 'CoreFeature'
        db.create_table('estimator_corefeature_legacy_pages', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('corefeature', models.ForeignKey(orm['estimator.corefeature'], null=False)),
            ('legacypage', models.ForeignKey(orm['estimator.legacypage'], null=False))
        ))
        db.create_unique('estimator_corefeature_legacy_pages', ['corefeature_id', 'legacypage_id'])

        # Adding model 'Requirement'
        db.create_table('estimator_requirement', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('core_feature', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estimator.CoreFeature'])),
            ('points', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('notes', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('deactivate', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('estimator', ['Requirement'])

        # Adding model 'EstimationMetric'
        db.create_table('estimator_estimationmetric', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hours_per_point', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('estimator', ['EstimationMetric'])

        # Adding model 'LegacyNode'
        db.create_table('estimator_legacynode', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('site_photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['estimator.LegacyNode'])),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('form', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal('estimator', ['LegacyNode'])

        # Adding model 'LegacyPage'
        db.create_table('estimator_legacypage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['estimator.LegacyPage'], null=True, blank=True)),
        ))
        db.send_create_signal('estimator', ['LegacyPage'])


    def backwards(self, orm):
        
        # Deleting model 'Component'
        db.delete_table('estimator_component')

        # Deleting model 'CoreFeature'
        db.delete_table('estimator_corefeature')

        # Removing M2M table for field legacy_pages on 'CoreFeature'
        db.delete_table('estimator_corefeature_legacy_pages')

        # Deleting model 'Requirement'
        db.delete_table('estimator_requirement')

        # Deleting model 'EstimationMetric'
        db.delete_table('estimator_estimationmetric')

        # Deleting model 'LegacyNode'
        db.delete_table('estimator_legacynode')

        # Deleting model 'LegacyPage'
        db.delete_table('estimator_legacypage')


    models = {
        'estimator.component': {
            'Meta': {'object_name': 'Component'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'estimator.corefeature': {
            'Meta': {'object_name': 'CoreFeature'},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['estimator.Component']"}),
            'deactivate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legacy_pages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['estimator.LegacyPage']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        },
        'estimator.estimationmetric': {
            'Meta': {'object_name': 'EstimationMetric'},
            'hours_per_point': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'estimator.legacynode': {
            'Meta': {'object_name': 'LegacyNode'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'form': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['estimator.LegacyNode']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'site_photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        'estimator.legacypage': {
            'Meta': {'object_name': 'LegacyPage'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['estimator.LegacyPage']", 'null': 'True', 'blank': 'True'})
        },
        'estimator.requirement': {
            'Meta': {'object_name': 'Requirement'},
            'core_feature': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['estimator.CoreFeature']"}),
            'deactivate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'points': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'})
        }
    }

    complete_apps = ['estimator']
