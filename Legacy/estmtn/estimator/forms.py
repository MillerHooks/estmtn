from django import forms
from estimator.models import Requirement, UserStory

class RequirementForm(forms.ModelForm):
    class Meta:
        model = Requirement
        exclude = ('core_feature')

class UserStoryForm(forms.ModelForm):
    id = forms.CharField(widget=forms.HiddenInput)
    class Meta:
        model = UserStory
        exclude = ('core_feature')
        fields = ('id', 'name', 'story_id', 'user_role', 'points', 'priority', 'notes', 'deactivate')