from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import settings

from estimator.views import DashboardView, FeatureView, \
    RequirementDetailView, LegacyPageTreeListView, \
    EstimateView, EstimationResultView, CSVExprotView, \
    PageTreeListView
    
from estimator.utils import export

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'reidfaist.views.home', name='home'),
    # url(r'^reidfaist/', include('reidfaist.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^accounts/', include('registration.backends.default.urls')),


    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', DashboardView.as_view(), name='dashboard'),
    url(r'^requirements/$', LegacyPageTreeListView.as_view(), name='legacy'),
    url(r'^feature/(\w+)/$', FeatureView.as_view(), name='feature'),

    url(r'^estimate/$', EstimateView.as_view(), name='estimate'),
    url(r'^estimate/(?P<pk>\d+)/$', EstimateView.as_view(), name='estimate'),
    url(r'^results/$', EstimationResultView.as_view(), name='results'),

    url(r'^requirement/(?P<pk>\d+)/$', RequirementDetailView.as_view(), name='requirement'),
    url(r'^export/(?P<app_label>\w+)/(?P<model_name>\w+)/(?P<format>\w+)/$', admin.site.admin_view(export), {'admin_site': admin.site}),

    url(r'^pages/$', PageTreeListView.as_view(), name='page'),

    url(r'^csv/$', CSVExprotView.as_view(), name='csv'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.map_path('media'),
        }),
)
